const config = require('./protractor.conf').config;
config.capabilities = {
    browserName: 'chrome',
    chromeOptions: {
        args: ['--headlesss','--no-sandbox']
    }
};
exports.config = config;
